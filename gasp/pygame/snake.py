from gasp import games
from gasp import color
from gasp import boards
from gasp.utils import random_between


BOX_SIZE = 40
MARGIN = 60

SNAKE_COLOR = color.LIGHTGREEN
EMPTY_COLOR = color.LIGHTGRAY

