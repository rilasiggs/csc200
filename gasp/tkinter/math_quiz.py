from random import randint

foo = 0
correct = 0

while foo != 10:
    foo += 1
    num1 = randint(1, 10)
    num2 = randint(1, 10)
    ans = num1 * num2
    print(f'what is {num1} times {num2}?')
    if ans == int(input()):
        print('That’s right – well done.')
        correct += 1
    else:
        print(f'No, I’m afraid the answer is {ans}.')
print(f'I asked you 10 questions. You got {correct} of them right')
print('Well done!')
