from gasp import *
from random import randint

def place_robot():
    global robot_x, robot_y, robot_shape
    robot_x = randint(0, 63)
    robot_y = randint(0, 47)
    robot_shape = Box((10 * robot_x + 5, 10 * robot_y + 5), 10, 10)

def place_player():
    global player_shape, player_x, player_y
    player_x = randint(0, 63)
    player_y = randint(0, 47)
    player_shape = Circle((10 * player_x - 5, 10 * player_y - 5), 5, filled=True)

def move_player():
    global player_shape, player_x, player_y
    key = update_when("key_pressed")
    if key == "d" and player_x < 63:
        player_x += 1
    elif key == "c":
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1
    elif key == "x" and player_y > 0:
        player_y -= 1
    elif key == "z":
        if player_x > 0:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
    elif key == "a" and player_x > 0:
        player_x -= 1
    elif key == "q":
        if player_x > 0:
            player_x -= 1
        if player_y < 47:
            player_y += 1
    elif key == "w" and player_y < 47:
        player_y += 1
    elif key == "e":
        if player_x < 63:
            player_x += 1
        if player_y < 47:
            player_y += 1
    elif key == "space":
        end_graphics()
    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def move_robot():
    global robot_x, robot_y, robot_shape
    if robot_x < player_x:
        robot_x += 1
    if robot_x > player_x:
        robot_x -= 1
    if robot_y < player_y:
        robot_y += 1
    if robot_y > player_y:
        robot_y -= 1
    move_to(robot_shape, (10 * robot_x + 5, 10 * robot_y + 5))

def check_collisions():
    if player_x == robot_x and player_y == robot_y:
        end_graphics()

begin_graphics()
finished = False

place_robot()
place_player()

while not finished:
    move_player()
    move_robot()
    check_collisions()

end_graphics()
