from random import randint

tries = 0
num = randint(1, 1000)
print('OK, I\'ve thought of a number between 1 and 1000.\n')
while True:
    guess = int(input('Make a guess: '))
    tries += 1
    if guess > num:
        print('That\'s too high.\n')
    elif guess < num:
        print('That\'s too low.\n')
    else:
        print('That was my number. Well done!\n')
        print(f'You took {tries} guesses.')
        another = input('Would you like another game? ')
        if another == 'yes':
            num = randint(1, 1000)
            print('OK, I\'ve thought of a number between 1 and 1000.\n')
        else:
            print('OK. Bye!')
            break
