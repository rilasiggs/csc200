import json

data = '''
[
    {
        "name" : "Silas Riggs",
        "address" : {
            "street number" : "5206",
            "street name" : "27th st N"
        },
        "phone number" : "703-533-0735",
        "fav color" : "Black"
    },
    {
        "name" : "Dr. Chuck",
        "address" : {
            "street number" : "1234",
            "street name" : "11th rd S"
        },
        "phone number" : "703-828-1234",
        "fav color" : "Pink"
    },
    {
        "name" : "Jeff Elkner",
        "address" : {
            "street number" : "6924",
            "street name" : "Washington ln E"
        },
        "phone number" : "703-834-0479",
        "fav color" : "Orange"
    }
]
'''

info = json.loads(data)
print("* * * * * * * * * * * * * * * * * * *")
for bigobj in info:
    print(bigobj["name"])
    print(bigobj["address"]["street number"], bigobj["address"]["street name"])
    print(bigobj["phone number"])
    print(bigobj["fav color"])
    print("* * * * * * * * * * * * * * * * * * *")
