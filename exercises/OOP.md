1. What is object-oriented programming (OOP)? When was it created, and why?
OOP was created by Alan Kay in 1961-1962 to create scopes/levels of access. OOP binds functions and data together in a way that only certain obbjects can access it.
2. What are the principle characteristics of OOP?
The key principles of OOP are encapsulation, abstraction, inheritance, and polymorphism.
3. How is OOP supported in Python?
Everything in python is an object, meaning they all hae methods and attributes. Python also allows users to create classes and define methods with ease.
